define({
  "name": "skyring",
  "version": "6.0.0-alpha.1",
  "description": "Distributed timers as a service",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-02-22T04:33:17.651Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
